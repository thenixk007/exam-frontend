import Vue from 'vue';
import {Skeleton, SkeletonTheme } from 'vue-loading-skeleton';
 
Vue.use(Skeleton, SkeletonTheme)